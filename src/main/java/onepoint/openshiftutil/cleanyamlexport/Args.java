package onepoint.openshiftutil.cleanyamlexport;

import java.io.File;
import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;

public class Args {
    @Parameter(description = "input-file", required = true, converter=FileConverter.class, arity=1)
    private List<File> input;

    @Parameter(names = "-o", description = "Output file", converter=FileConverter.class)
    private File output;

    public List<File> getInput() {
        return input;
    }

    public void setInput(List<File> input) {
        this.input = input;
    }

    public File getOutput() {
        return output;
    }

    public void setOutput(File output) {
        this.output = output;
    }
}