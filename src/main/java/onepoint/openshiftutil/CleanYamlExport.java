package onepoint.openshiftutil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import onepoint.openshiftutil.cleanyamlexport.Args;

public class CleanYamlExport {
    @SuppressWarnings("rawtypes")
    public static void main(String[] argv) {
        final Args args = new Args();
        JCommander jc = JCommander.newBuilder().addObject(args).programName(CleanYamlExport.class.getName())
                .build();

        try {

            jc.parse(argv);
            Yaml yameule = new Yaml(new SafeConstructor());
            try (InputStream in = new FileInputStream(args.getInput().get(0)); Writer out = openOut(args)) {
                Map<String, Object> data = yameule.load(in);
                Set<String> names = new HashSet<>();
                clean("/", data, names);
                List<String> sortedNames = sortAndFilterNames(names);

                Map<String, String> usedParams = new HashMap<>();
                
                parameterize(data, sortedNames, usedParams);
                
                if(!usedParams.isEmpty()) {
                    List<Map> parameters = new ArrayList<>();
                    for(Map.Entry<String, String> entry : usedParams.entrySet()) {
                        Map<String, String> parameter = new LinkedHashMap<>();
                        parameter.put("name", entry.getKey());
                        parameter.put("displayName", "");
                        parameter.put("description", "");
                        parameter.put("value", entry.getValue());
                        parameters.add(parameter);
                    }
                    data.put("parameters", parameters);
                }
                
                yameule.dump(data, out);
                
            } catch (IOException e) {
                System.err.println(e.getMessage());
                System.exit(2);
            }
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            jc.usage();
            System.exit(1);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void parameterize(Map<String, Object> data, List<String> sortedNames,
            Map<String, String> usedParams) {
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            if (entry.getValue() instanceof String) {
                entry.setValue(parameterize((String) entry.getValue(), sortedNames, usedParams));
            } else if (entry.getValue() instanceof Map) {
                parameterize((Map) entry.getValue(), sortedNames, usedParams);
            } else if (entry.getValue() instanceof List) {
                List things = (List) entry.getValue();
                for (int i = 0; i < things.size(); ++i) {
                    Object thing = things.get(i);
                    if (thing instanceof String) {
                        things.set(i, parameterize((String) thing, sortedNames, usedParams));
                    } else if (thing instanceof Map) {
                        parameterize((Map) thing, sortedNames, usedParams);
                    }
                }
            }
        }
    }

    private static String parameterize(String value, List<String> sortedNames, Map<String, String> usedParams) {
        String result = value;

        for (String name : sortedNames) {
            result = parameterize(result, name, usedParams);
        }

        return result;
    }

    private static String parameterize(String string, String val, Map<String, String> usedParams) {
        if (!string.contains(val)) { return string; }
        StringBuilder result = new StringBuilder();
        int base = 0;
        int offset;
        do {
            offset = string.indexOf("${", base);
            String chunk;
            if (offset > -1) {
                chunk = string.substring(base, offset);
            } else {
                chunk = string.substring(base);
            }
            if (chunk.contains(val)) {
                String param = paramName(val);
                String regexp = val.replaceAll("\\.", "\\\\.");
                // System.err.println(regexp + " // " + chunk);
                try {
                    chunk = chunk.replaceAll(regexp, "\\${" + param + "}");
                    usedParams.put(param, val);
                } catch (IllegalArgumentException e) {
                    System.err.println("Invalid regexp " + regexp + " for " + chunk);
                    throw e;
                }
            }

            result.append(chunk);
            if (offset > -1) {
                base = string.indexOf('}', offset) + 1;
                result.append(string.substring(offset, base));
                if (base == string.length()) {
                    offset = -1;
                }
            }
        } while (offset > -1);

        return result.toString();
    }

    private static String paramName(String name) {
        return name.toUpperCase().replaceAll("\\.", "___").replaceAll("-", "__").replace('\'', 'q');
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static boolean clean(String prefix, Map<String, Object> data, Set<String> names) {
        boolean removeObject = false;
        all: for (Iterator<Map.Entry<String, Object>> it = data.entrySet().iterator(); it.hasNext();) {
            Entry<String, Object> entry = it.next();
            String k = entry.getKey();
            switch (k) {
            case "generation":
            case "status":
            case "ownerReferences":
            case "creationTimestamp":
                it.remove();
                continue;
            case "kind":
                if ("Build".equals(entry.getValue()) || "ReplicationController".equals(entry.getValue())
                        || "Pod".equals(entry.getValue())) {
                    removeObject = true;
                    break all;
                }
                continue;
            case "annotations":
                cleanAnnotations((Map) entry.getValue());
                break;
            }
            if (entry.getValue() instanceof Map && ((Map) entry.getValue()).isEmpty()) {
                it.remove();
                continue;
            }
            if (("name".equals(k) || "namespace".equals(k) || "secret".equals(k)) && !prefix.contains("ports")
                    && !prefix.contains("tags") && entry.getValue() instanceof String) {
                names.addAll(Arrays.asList(((String) entry.getValue()).split("[:/]")));
            }
            if (entry.getValue() instanceof Map) {
                clean(prefix + "/" + k, (Map) entry.getValue(), names);
            } else if (entry.getValue() instanceof List) {
                List things = (List) entry.getValue();
                final String thingPref = prefix + "/" + k + "[]";
                for (Iterator thingsIt = things.iterator(); thingsIt.hasNext();) {
                    Object thing = thingsIt.next();
                    if (thing instanceof Map) {
                        if (clean(thingPref, (Map) thing, names)) {
                            thingsIt.remove();
                        }
                    }
                }
            }
        }
        return removeObject;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void cleanAnnotations(Map annotations) {
        if (annotations != null) {
            for (Iterator<Map.Entry<String, String>> it = annotations.entrySet().iterator(); it.hasNext();) {
                Map.Entry<String, String> entry = it.next();
                String key = entry.getKey();
                if ("openshift.io/host.generated".equals(key) || "kubernetes.io/created-by".equals(key)
                        || "openshift.io/generated-by".equals(key)
                        || "openshift.io/encoded-deployment-config".equals(key)
                        || key.startsWith("openshift.io/deploy")
                        || key.startsWith("kubectl.kubernetes.io/original-")) {
                    it.remove();
                }
            }
        }
    }

    private static Writer openOut(Args args) throws FileNotFoundException, UnsupportedEncodingException {
        return new OutputStreamWriter(
                args.getOutput() != null ? new FileOutputStream(args.getOutput()) : System.out, "UTF-8");
    }

    private static List<String> sortAndFilterNames(Set<String> names) {
        List<String> result = new ArrayList<>();
        for (String s : names) {
            if (!isNumber(s) && !"openshift".equals(s)) {
                result.add(s);
            }
        }
        Collections.sort(result, (String a, String b) -> {
            int r = a.length() - b.length();
            if (r == 0) {
                r = a.compareTo(b);
            }
            return r;
        });

        return result;
    }

    private static boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
